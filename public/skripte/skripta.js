/* global L, $ */

// seznam z markerji na mapi
var markerji = [];

var mapa;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
 var counter=0;
window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 9,
    maxZoom: 12
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Akcija ob kliku na gumb za prikaz zgodovine
  $("#prikaziZgodovino").click(function() {
    window.location.href = "/zgodovina";
  });

  function obKlikuNaMapo(e) {
    // Izbriši prejšnje oznake
    for (var i = 0; i < markerji.length; i++) {
      mapa.removeLayer(markerji[i]);
    }

    var koordinate = e.latlng;

    // Dodaj trenutno oznako
    dodajMarker(koordinate.lat, koordinate.lng,
      "<b>Izbrana lokacija</b><br>(" + koordinate.lat + ", " + koordinate.lng + ")");

    // Prikaži vremenske podatke najbližjega kraja
    

    
  
    // Prikaži seznam 5 najbližjih krajev
    var lat=e.latlng.lat;
    var lng=e.latlng.lng;
    var kraji=[];
    $.getJSON("https://api.lavbic.net/kraji/lokacija?lat="+lat+"&lng="+lng,function(data){
      kraji=data;
      var prvi;
      var drugi;
      var tretji;
      var četrti;
      var peti;
      for(var i=0;i<5;i++){
        if(i==0){
          prvi="<b>"+kraji[i].postnaStevilka+"</b> "+kraji[i].kraj;
          console.log(prvi);
        }else if(i==1){
          drugi="<b>"+kraji[i].postnaStevilka+"</b> "+kraji[i].kraj;
        }else if(i==2){
          tretji="<b>"+kraji[i].postnaStevilka+"</b> "+kraji[i].kraj;
        }else if(i==3){
          četrti="<b>"+kraji[i].postnaStevilka+"</b> "+kraji[i].kraj;
        }else if(i==4){
          peti="<b>"+kraji[i].postnaStevilka+"</b> "+kraji[i].kraj;
        }
      }
      $("#bliznjiKraji").html("<p><h3>Bližnji kraji</h3><br>"+prvi+"<br>"+drugi+"<br>"+tretji+"<br>"+četrti+"<br>"+peti+"</p>");
      console.log(kraji[0].postnaStevilka);
      $.get(/vreme/+kraji[0].postnaStevilka,function(zahteva, odgovor) {
        console.log(zahteva);
        var opisPrvega=zahteva.opis;
        var ikonaPrvega="<img src=/slike/"+zahteva.ikona+">";
        var temperaturaPrvega=zahteva.temperatura;
        $("#vremeOkolica").html("<p><h3>Vreme v okolici kraja<br>"+kraji[0].kraj+"</h3>"+ikonaPrvega+" "+opisPrvega+" "+temperaturaPrvega+"<br></p>");
      
        $.get('/zabelezi-zadnji-kraj/' + JSON.stringify({
          postnaStevilka: kraji[0].postnaStevilka,
          kraj: kraji[0].kraj,
          vreme: opisPrvega,
          temperatura: temperaturaPrvega
          
         }), function(t) {
           counter++;
      $("#prikaziZgodovino").html("Prikaži zgodovino "+counter+" krajev");
    });
      });
        
    });
    // Shrani podatke najbližjega kraja
    
    
  }

  mapa.on('click', obKlikuNaMapo);

  // Na začetku postavi lokacijo na FRI
  var FRIpoint = new L.LatLng(FRI_LAT, FRI_LNG);
  mapa.fireEvent('click', {
    latlng: FRIpoint,
    layerPoint: mapa.latLngToLayerPoint(FRIpoint),
    containerPoint: mapa.latLngToContainerPoint(FRIpoint)
  });
});


/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah
 *
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 */
function dodajMarker(lat, lng, opis) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-icon-2x-blue.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  
  var marker = L.marker([lat, lng], {icon: ikona});
  marker.bindPopup("<div>" + opis + "</div>").openPopup();
  marker.addTo(mapa);
  markerji.push(marker);
  
}
